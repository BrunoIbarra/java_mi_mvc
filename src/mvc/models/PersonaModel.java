package mvc.models;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;

public class PersonaModel implements Modelo{
    private String id = null;        
    private String nombre;
    private String apellido;
    private String direccion; 
    private String cargo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
    
    // INICIO DE LA API
    
    @Override
    public int post(){
        int retorno = 0;
        
        Connection connection = MysqlConnection.getConnection();
        
        String consulta = "insert into persona values( null, ?, ?, ?, ? )";
        
        try {
            
            PreparedStatement ps = connection.prepareStatement(consulta);
           
            ps.setString(1, this.nombre);
            ps.setString(2, this.apellido);
            ps.setString(3, this.direccion);
            ps.setString(4, this.cargo);
            
            retorno = ps.executeUpdate();
            
        } catch (SQLException ex) {
            //
        }finally{
            closeThisConnection(connection);
        }
        
        return retorno;
    }
    
    @Override
    public int put(){
        int retorno = 0;
        
        Connection connection = MysqlConnection.getConnection();
        
        String consulta = "update persona "
                + "set nombre = ?"
                + ", apellido = ?"
                + ", direccion = ?"
                + ", cargo = ?"
                + "where id = ?";
        
        
        
        try {
            
            PreparedStatement ps = connection.prepareStatement(consulta);
           
            ps.setString(1, this.nombre);
            ps.setString(2, this.apellido);
            ps.setString(3, this.direccion);
            ps.setString(4, this.cargo);
            ps.setInt(5, Integer.parseInt(this.id));
            
            retorno = ps.executeUpdate();
            
        } catch (SQLException ex) {
            //
        }finally{
            closeThisConnection(connection);
        }
        
        return retorno;
    }
    
    @Override
    public int delete(){
        int retorno = 0;
        
        Connection connection = MysqlConnection.getConnection();
        
        String consulta = "delete from persona "
                + "where id = ?";

        try {
            
            PreparedStatement ps = connection.prepareStatement(consulta);
           
            ps.setInt(1, Integer.parseInt(this.id));
            
            retorno = ps.executeUpdate();
            
        } catch (SQLException ex) {
            //
        }finally{
            closeThisConnection(connection);
        }
        
        return retorno;
    }
    
    public void closeThisConnection(Connection con){
        try {
            con.close();
        } catch (SQLException e) {
            // todo
        }
    }
    
    // Fin API
 
    /**
     * Este método prepara el modelo de de la tabla, con todas las personas.
     * 
     * @return DefaultTableModel modeloTabla.
     */
    public DefaultTableModel getAllTableModel(){
        Connection conn = MysqlConnection.getConnection();
        String consulta = ""
                +"select    id"
                + "         ,nombre"
                + "         ,apellido"
                + "         ,direccion"
                + "         ,cargo "
                + "from     persona;";
        
        DefaultTableModel modeloTabla = new DefaultTableModel();

        modeloTabla.addColumn("ID");
        modeloTabla.addColumn("NOMBRE");
        modeloTabla.addColumn("APELLIDO");
        modeloTabla.addColumn("DIRECCIÓN");
        modeloTabla.addColumn("CARGO");
        
        int numcolumnas = modeloTabla.getColumnCount();
        
        try {
            PreparedStatement ps = conn.prepareStatement(consulta);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Object[] fila = new Object[numcolumnas];
                
                for(int i = 0; i < numcolumnas; i++){
                    fila[i] = rs.getObject(i + 1);
                }
                
                modeloTabla.addRow(fila);
                
            }
        } catch (SQLException ex) {
            System.out.println("Error en cargaTodasEnTabla: " + ex);
                    
        } finally{
            closeThisConnection(conn);
        }
        return modeloTabla;
    }
    
    @Override
    public String muestraAtributos(){
        return ""
                + "- Id: " + id + "\n"
                + "- Nombre: " + nombre + "\n"
                + "- Apellido: " + apellido + "\n"
                + "- Direccion: " + direccion + "\n"
                + "- Cargo: " + cargo;
    }
    
}
