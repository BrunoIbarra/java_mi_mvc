package mvc.models;

public interface Modelo {
    
    public int post();
    
    public int put();
    
    public int delete();
    
    public String muestraAtributos();
}
